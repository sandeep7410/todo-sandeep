var counter = 0;
var source=null;
var allElements=document.getElementsByClassName("all");
var activeElements=document.getElementsByClassName("active");
var completedElements=document.getElementsByClassName("completed");
var countActive=null;
function countActive(){
    countActive=document.getElementsByClassName("active");
    let noOfActive=countActive.length;
    let replace=document.getElementById("count");
    if(noOfActive>1){
        replace.textContent=noOfActive+" items";
    }
    else{
        replace.textContent=noOfActive+"item";
    }
    
}
function addPara(event) {
    let text = document.getElementById("input").value;
    if (event.keyCode == 13 && text.length != 0) {
        let div = document.createElement("div");
        let check = document.createElement("input");
        check.setAttribute("type", "checkbox");
        check.classList.add("checker");
        div.appendChild(check);

        let p = document.createElement("p");
        p.textContent = text;
        // let p=document.createElement("input");
        // p.value=text;
        document.getElementById("input").value = null;
        // p.setAttribute("disabled","");
        div.appendChild(p);
        p.addEventListener("dblclick", editPara);
        check.addEventListener("click", strikePara);

        let remove = document.createElement("span");
        remove.setAttribute("class", "delete");
        remove.innerHTML = " &#10005;";
        div.appendChild(remove);
        div.classList.add("all");
        div.classList.add("active");
        remove.addEventListener("click", removePara);
        document.querySelector("main").appendChild(div);
        div.setAttribute("draggable", "true");
        div.setAttribute("id", counter);
        counter++;
        div.addEventListener("dragstart", handleDragStart);
        div.addEventListener("dragover", handleDragOver);
        div.addEventListener("dragenter", handleDragEnter);
        div.addEventListener("dragleave", handleDragLeave);
        div.addEventListener("drop", handleDrop);
        div.addEventListener("dragend",handleDragEnd);
        if(document.getElementsByClassName("all").length>0){
            let bottom=document.getElementById("bottom");
            bottom.style.display="initial";
        }
        //countActive();
    }
}

function removePara(event) {
    // event.target.parentNode.parentNode.removeChild(event.target.parentNode);
    event.target.parentNode.remove();
   // countActive();
}

function strikePara(event) {
    let parent = event.target.parentNode;
    let para = parent.querySelector("p");
    if (event.target.checked == true) {
        para.style.textDecoration = "line-through";
        parent.classList.remove("active");
        parent.classList.add("completed");
    } else {
        para.style.textDecoration = "initial";
        parent.classList.remove("completed");
        parent.classList.add("active");
    }
    //countActive();
}
document.getElementById("input").addEventListener("keyup", addPara);

function editPara(event) {
    let p = event.target;
    event.target.removeAttribute("disabled");

}
//var dragSrcEl = null;

function handleDragStart(event) {
    source = event.target;
    event.target.style.backgroundColor="black";
    event.target.style.color="white";
   // dragSrcEl = this;
    console.log(this);
    event.dataTransfer.effectAllowed = "move";
    console.log(event.target);
   // event.dataTransfer.setData("text/html",this);
}

function handleDragOver(event) {
    event.preventDefault();
    let div = event.target;
    event.dataTransfer.dropEffect = "move";
    console.log(event);
}

function handleDragEnter(event) {
    this.style.backgroundColor="grey";
}

function handleDragLeave(event) {
   this.style.backgroundColor="white";
}
function handleDragEnd(event){
    this.style.backgroundColor="white";
    this.style.color="black";
}

function handleDrop(event) {
    this.style.backgroundColor="white";
    // event.target.insertAdjacentHTML("afterend",event.dataTransfer.getData("text/html"));
    // dragSrcEl.innerHTML = this.innerHTML;
    // this.innerHTML = event.dataTransfer.getData("text/html");
    let h=event.target.offsetHeight;
    if(event.offsetY>h/2){
        this.parentNode.insertBefore(source, this.nextSibling);
    }
    else{
        this.parentNode.insertBefore(source, this);
    }
}

function allone(event){
    let hiddenElements=document.querySelectorAll(".hidden");
    console.log(hiddenElements);
    for(let i=hiddenElements.length-1;i>=0;i--){
        hiddenElements[i].classList.remove("hidden");
    }
}
function active(event){
    for(let i=0;i<activeElements.length;i++){
        activeElements[i].classList.remove("hidden");
    }
    for(let i=0;i<completedElements.length;i++){
        completedElements[i].classList.add("hidden");
    }
}
function completed(event){
    for(let i=0;i<completedElements.length;i++){
        completedElements[i].classList.remove("hidden");
    }
    for(let i=0;i<activeElements.length;i++){
        activeElements[i].classList.add("hidden");
    }
    
}
function clearCompleted(event){
    let completed=document.querySelectorAll(".completed");
    console.log(completed);
    for(let i=completed.length-1;i>=0;i--){
        completed[i].remove();
    }
}